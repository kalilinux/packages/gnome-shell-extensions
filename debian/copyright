Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://download.gnome.org/sources/gnome-shell-extensions/

Files: *
Copyright: 2011-2013 Giovanni Campagna
           2013-2024 Florian Müllner
           2011 Iranian Free Software Users Group (IFSUG.org) translation team
           2021-2022 Alexander Shopov
           2011-2023 Red Hat, Inc
           2013 Rosetta Contributors and Canonical Ltd
           Free Software Foundation, Inc
           Rūdofls Mazurs
           Ville-Pekka Vainio
License: GPL-2+

Files: debian/*
Copyright: 2011 Victor Seva <linuxmaniac@torreviejawireless.org>
           2011 Bilal Akhtar <bilalakhtar@ubuntu.com>
License: GPL-2+

Files: extensions/apps-menu/extension.js
Copyright: 2011 Giovanni Campagna
           2011 Vamsi Krishna Brahmajosyula
           2013 Debarshi Ray
           2013 Florian Müllner
License: GPL-2+

Files: extensions/auto-move-windows/extension.js
Copyright: 2011 Alessandro Crismani
           2011 Giovanni Campagna
           2014 Florian Müllner
License: GPL-2+

Files: extensions/launch-new-instance/extension.js
Copyright: 2013 Florian Müllner
           2013 Gabriel Rossetti
License: GPL-2+

Files: extensions/native-window-placement/extension.js
Copyright: 2011 Giovanni Campagna
           2011 Stefano Facchini
           2011 Wepmaschda
           2015 Florian Müllner 
License: GPL-2+

Files: extensions/places-menu/extension.js
Copyright: 2011 Giovanni Campagna
           2011 Vamsi Krishna Brahmajosyula
           2013 Florian Müllner
           2016 Rémy Lefevre
License: GPL-2+

Files: extensions/places-menu/placeDisplay.js
Copyright: 2012 Giovanni Campagna
           2013 Debarshi Ray
           2015 Florian Müllner
           2016 Rémy Lefevre
           2017 Christian Kellner
License: GPL-2+

Files: extensions/screenshot-window-sizer/extension.js
Copyright: 2013 Owen Taylor
           2013 Richard Hughes
           2014 Florian Müllner
           2016 Will Thompson
           2017 Florian Müllner
           2019 Adrien Plazas
           2019 Willy Stadnick
License: GPL-2+

Files: extensions/status-icons/extension.js
Copyright: 2018 Adel Gadllah
           2018 Florian Müllner
License: GPL-2+

Files: extensions/system-monitor/icons/*
Copyright: GNOME Design Team Icon Development Kit
License: CC0-1.0

Files: extensions/user-theme/extension.js
Copyright: 2011 Elad Alfassa
           2011 Giovanni Campagna
           2011 John Stowers
           2014 Florian Müllner
License: GPL-2+

Files: extensions/window-list/prefs.js
Copyright: 2013 Florian Müllner
           2014 Sylvain Pasche
License: GPL-2+

Files: extensions/window-list/stylesheet-light.css
       extensions/workspace-indicator/stylesheet-light.css
Copyright: 2011-2013 Giovanni Campagna
           2013-2024 Florian Müllner
License: GPL-2+

Files: extensions/windowsNavigator/extension.js
Copyright: 2011 Giovanni Campagna
           2011, Maxim Ermilov
           2017, Florian Müllner
           2019, Marco Trevisan (Treviño)
           2020, Thun Pin
License: GPL-2+

Files: extensions/windowsNavigator/stylesheet.css
Copyright: 2011 Maxim Ermilov
License: GPL-2+

Files: extensions/workspace-indicator/stylesheet-dark.css
Copyright: 2011 Erick Pérez Castellanos
           2019 Florian Müllner
License: GPL-2+

Files: lint/eslintrc-gjs.yml
Copyright: 2018 Claudio André
License: Expat

Files: lint/eslintrc-shell.yml
Copyright: 2019 Florian Müllner
License: Expat

Files: meson/session-post-install.py
Copyright: 2021, Neal Gompa
License: GPL-2+

License: CC0-1.0
 To the extent possible under law, the author(s) have dedicated all copyright
 and related and neighboring rights to this software to the public domain
 worldwide. This software is distributed without any warranty.
 .
 You should have received a copy of the CC0 Public Domain Dedication along with
 this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
 .
 On Debian systems, the complete text of the CC0 1.0 Universal license can be
 found in "/usr/share/common-licenses/CC0-1.0".

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".
